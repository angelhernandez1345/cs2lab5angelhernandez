package edu.westga.cs1302.casino.test.baccarat;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.casino.game.Baccarat;
import edu.westga.cs1302.casino.model.Card;
import edu.westga.cs1302.casino.model.HumanPlayer;
import edu.westga.cs1302.casino.model.Rank;
import edu.westga.cs1302.casino.model.Suit;

/**
 * Ensures correct functionality of the getScore method.
 *  
 * @author Angel Hernandez
 * @version 10/02/2021
 *
 */
public class TestGetScore {

	@Test
	public void testValidInput() {
		Baccarat newGame = new Baccarat();
		HumanPlayer one = new HumanPlayer(Baccarat.HUMAN_PLAYER_MONEY);
		Card first = new Card(Rank.EIGHT, Suit.CLUBS);
		Card second = new Card(Rank.ACE, Suit.CLUBS);
		Card third = new Card(Rank.TWO, Suit.CLUBS);

		one.addCard(first);
		one.addCard(second);
		one.addCard(third);
		
		int result = one.getNumCardsInHand();
		int score = newGame.getScore(one.getHand());
		
		assertEquals(2, result);
		assertEquals(3, score);
	}

}
