package edu.westga.cs1302.casino.game;

import java.util.ArrayList;

import edu.westga.cs1302.casino.model.Card;
import edu.westga.cs1302.casino.model.Player;

/**
 * The Blackjack class.
 * 
 * @author Angel Hernandez
 * @version 10/02/2021
 *
 */
public class Blackjack extends Game {
	
	public static final int DEALER_THRESHOLD = 17;

	/**
	 * Instantiates a game and starts a new round.
	 * 
	 * @precondition none
	 * @postcondition a new game is started
	 */
	public Blackjack() {
		super();
	}
	
	@Override
	/**
	 * Starts a new round of the game.
	 * 
	 * @precondition none
	 * @postcondition deck is instantiated and shuffled && getPot() == 0 && the
	 *                players have no cards
	 */
	public void startNewRound() {
		super.startNewRound();
		this.setMessage(GameType.BLACKJACK.toString());
	}

	@Override
	/**
	 * Checks if the specified hand is a "Natural" according to the Blackjack rules.
	 * A "Natural" is a hand that has one face card and one Ace card.
	 * 
	 * @precondition none
	 * @postcondition none
	 * @param hand an array list of cards representing the player's hand
	 * @return true if hand is natural, false otherwise
	 */
	public boolean isNatural(ArrayList<Card> hand) {
		// TO DO
		for (Card current : hand) {
			if (current.getRank() == 1) {
				if (current.isFaceCard()) {
					return true;
				}
			}
		}	
		return false;
	}

	@Override
	/**
	 * Hit: player asks for an additional card from the deck.
	 * 
	 * @precondition none
	 * @postcondition one card was dealt from the deck and added to the player's
	 *                hand; if the player busts, the dealer wins the round and the
	 *                message of the game is "Bust - you lose."
	 * @return false if player busted, true otherwise
	 */
	public boolean hit() {
		Player human = this.getHumanPlayer();
		this.dealCardTo(human);
		int humanScore = this.getScore(human.getHand());
		
		if (humanScore > 21) {
			this.dealerWinsRound("Bust - you lose.");
			return false;
		} else {
			return true;
		}
	}

	@Override
	/**
	 * The player has finished their turn and not busted, so now they stand. The
	 * dealer plays their turn and the game ends.
	 * 
	 * @precondition the player stands
	 * @postcondition the game reached a resolution
	 */
	public void humanPlayerStands() {
		// TO DO
	}

	@Override
	/**
	 * Returns the hand score.
	 * 
	 * @preconditon none
	 * @postcondition none
	 * 
	 * @param hand an array list of cards representing the player's hand
	 * @return the score
	 */
	public int getScore(ArrayList<Card> hand) {
		int score = 0;		
		for (Card current : hand) {
			if (!(current.isFaceCard() || current.getRank() == 1)) {
				score += current.getRank();
			}
			if (current.isFaceCard()) {
				score += 10;
			} 		
			if (current.getRank() == 1) {
				if (score <= 10) {
					score += 11;
				} else {
					score += 1;	
				}		
			}
			
		}
		return score;
	}
}